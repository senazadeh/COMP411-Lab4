/* Example: bubble sort strings in array */

#include <stdio.h>  /* Need for standard I/O functions */
#include <string.h> /* Need for strlen() */


#define NUM 25   /* number of strings */
#define LEN 1000  /* max length of each string */


int my_compare_strings(char string1[], char string2[]) {

    for (int i = 0; i < strlen(string1) -1; i++) {
        if (string1[i] == string2[i]) {
            continue;
        }
        if (string1[i] < string2[i]) {
            return -1;
        }
        if (string1[i] > string2[i]) {
            return 1;   
        }
    }
    if (strlen(string1) < strlen(string2)) {
        return -1;
    } 
    if (strlen(string1) > strlen(string2)) {
        return 1;
    }
    return 0;
}

void my_swap_strings(char string1[], char string2[]) {
  
  char temp;    
  for (int i = 0; i < LEN; i++) {
      temp = string1[i];
      string1[i] = string2[i];
      string2[i] = temp;
  }
}


int main()
{
  char Strings[NUM][LEN];
int outer, inner;
  printf("Please enter %d strings, one per line:\n", NUM);

    for (int i = 0; i < NUM; i++) {
        fgets(Strings[i], LEN, stdin); 
    }

  puts("\nHere are the strings in the order you entered:");
  for (int i = 0; i < NUM; i++) {
    printf("%s", Strings[i]);
  }

    for (outer = 0; outer < NUM - 1; outer++) {
        int didSwap = 0;
        for (inner = outer; inner < NUM; inner++) {;
            if (my_compare_strings(Strings[outer], Strings[inner]) == 1) {
                my_swap_strings(Strings[outer], Strings[inner]);
                didSwap = 1;
            }
        }
        if (didSwap == 0) {
            break;
        }
    }
  
  puts("\nIn alphabetical order, the strings are:");  
  for (int i = 0; i < NUM; i++) {
    printf("%s", Strings[i]);
  }
}
